package org.grechishnikov.Lab2.interfaces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("org.grechishnikov.Lab2.interfaces")
public class JavaConfig {

}
