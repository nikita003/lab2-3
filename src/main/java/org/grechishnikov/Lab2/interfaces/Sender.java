package org.grechishnikov.Lab2.interfaces;

public interface Sender {
    public String sendMessage(String message);
}
