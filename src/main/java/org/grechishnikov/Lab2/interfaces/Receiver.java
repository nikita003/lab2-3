package org.grechishnikov.Lab2.interfaces;

public interface Receiver {
    public String getMessage(String message);
}
